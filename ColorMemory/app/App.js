import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import RootNavigator from './navigators/RootNavigtor';

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <RootNavigator />
            </Provider>
        );
    }
}