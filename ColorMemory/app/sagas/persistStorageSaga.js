import {
    takeLatest, call, put, select,
  } from 'redux-saga/effects';
  import { AsyncStorage } from 'react-native';
  import { fromJS } from 'immutable';
  
  import {
    READ_PERSISTED_STORAGE,
    READ_PERSISTED_STORAGE_SUCCESS,
    SET_PERSISTED_STORAGE,
    PERSIST_SCORES,
  } from '../actions/score';
  
  import { selectScores } from '../selectors/data';
  /**
   * Manage the data persist between app launch.
   * e.g. scores 
   *
   * Items are read when app launched.
   * Items are write async when redux store change
   */
  const LocalScoresPersistKey = '@PersistStore:localScores';
  
  function* writeLocalScoresToStore() {
    const localScoresJson = yield AsyncStorage.getItem(LocalScoresPersistKey);
    const scores = localScoresJson && JSON.parse(localScoresJson) || [];
    
    yield put({ type: SET_PERSISTED_STORAGE, scores: fromJS(scores) });
  }
  
  function* readPersistedStorage() {
    // read from async store and write to store
    yield call(writeLocalScoresToStore);
    yield put({ type: READ_PERSISTED_STORAGE_SUCCESS });
  }
  
  export function* readPersistedStorageWatcher() {
    yield takeLatest(READ_PERSISTED_STORAGE, readPersistedStorage);
  }
  
  function* updatePersistedScores() {
      const scores = yield select(selectScores);
      if (scores != null) {
        const scoresJSON = JSON.stringify(scores);
        yield AsyncStorage.setItem(LocalScoresPersistKey, scoresJSON);
      } else {
        yield AsyncStorage.removeItem(LocalScoresPersistKey);
      }
  }
  
  export function* writePersistedStorageWatcher() {
    yield takeLatest(PERSIST_SCORES, updatePersistedScores);
  }
  
  export default [readPersistedStorageWatcher(), writePersistedStorageWatcher()];
  