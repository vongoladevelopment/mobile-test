import { takeLatest, put, select } from 'redux-saga/effects';
import { Map, List } from 'immutable';
import { ADD_SCORE, ADD_SCORE_SUCCESS, ADD_SCORE_FAIL, PERSIST_SCORES } from '../actions/score';
import { selectScores } from '../selectors/data';

const sortAccending = (a, b) => {
    console.log(a, b)
    if (a < b) return 1;
    if (a > b) return -1;
    return 0; 
}

function* addScore(action) {
    try {
        const { name, score } = action;
        const currentScores = yield select(selectScores);
        let scores = currentScores.push(Map({ name, score }));
        scores = scores.sortBy(item => item.get('score'), sortAccending);
        
        // Add rank to each score item.
        let previousScore;
        let previousRank;
        scores = scores.reduce((acc, item, index) => {
            console.log(acc.toJSON())
            let newItem;
            if (index === 0) {
                previousScore = item.get('score');
                previousRank = 1;
            } else {
                if (previousScore === item.get('score')) {
                    
                } else {
                    previousScore = item.get('score');
                    previousRank += 1;
                }
            }
            newItem = item.set('rank', previousRank);
            return acc.push(newItem);
        }, List());

        yield put({ type: ADD_SCORE_SUCCESS, scores });
        yield put({ type: PERSIST_SCORES });
    } catch (error) {
        yield put({ type: ADD_SCORE_FAIL, error });
    }
}

export function* addScoreWatcher() {
    yield takeLatest(ADD_SCORE, addScore);
}


export default [
    addScoreWatcher(),
];