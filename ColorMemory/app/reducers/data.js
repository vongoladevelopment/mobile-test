import { fromJS } from 'immutable';
import { ADD_SCORE_SUCCESS, SET_PERSISTED_STORAGE } from '../actions/score';

const initialState = fromJS({
    scores: [],
});

const actionsMap = {
  [SET_PERSISTED_STORAGE]: (state, action) => state.set('scores', action.scores),
  [ADD_SCORE_SUCCESS]: (state, action) => state.set('scores', action.scores),
};

export default function reducer(state = initialState, action = {}) {
  const fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}
