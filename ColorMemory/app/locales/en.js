export default {
    title__high_score: 'High Scores',
    game__label_score: 'Scores:',
    game__prompt_title: 'Finish',
    game__prompt_description: 'Please enter your name.',

    button__high_scores: 'High\nScores',
    button__submit: 'Submit',
    button__cancel: 'Cancel',

    high_scores__name: 'Name',
    high_scores__rank: 'Rank',
    high_scores__score: 'Score',
    high_scores__placeholder: 'No score is found.\nStart a game now.',

}