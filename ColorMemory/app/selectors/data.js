import { createSelector } from 'reselect';

export const selectData = state => state.data;

export const selectScores = createSelector(
    selectData,
    data => data.get('scores'),
);