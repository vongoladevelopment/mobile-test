import React from 'react';
import { 
    View,
    KeyboardAvoidingView,
    Platform,
    TextInput,
    Text,
    StyleSheet,
    Modal,
    TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types';
import EN from '../locales/en';
const COLOR = Platform.OS === 'ios' ? '#007ff9' : '#169689';

class InputDialog extends React.PureComponent {
  render() {
    const {
        visible,
        title,
        description,
        placeholder,
        onChangeText,
        onCancelPress,
        onSubmitPress,
        disableSubmit,
        labelSubmit,
        labelCancel
    } = this.props;

    return (
      <Modal
        transparent
        visible={visible}
        onRequestClose={() => {}}
        supportedOrientations={['portrait', 'landscape']}
      >
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : undefined}
          style={styles.container}
        >
          <View style={styles.content}>
            {Platform.OS === 'ios' && <View style={styles.blur} />}
            <View style={styles.header}>
              <Text style={styles.title}>{title}</Text>
              <Text style={styles.description}>{description}</Text>
            </View>
            <View style={styles.textInputWrapper}>
              <TextInput
                style={[styles.textInput]}
                placeholder={placeholder}
                onChangeText={onChangeText}
                underlineColorAndroid={COLOR}
              />
            </View>
            <View style={styles.footer}>
              <TouchableOpacity
                style={styles.button}
                onPress={onCancelPress}
              >
                <Text style={[styles.buttonText]}>
                  {Platform.OS === 'ios' ? labelCancel : labelCancel.toUpperCase()}
                </Text>
              </TouchableOpacity>
              {Platform.OS === 'ios' && <View style={styles.buttonSeparator} />}
              <TouchableOpacity
                style={styles.button}
                onPress={onSubmitPress}
                disabled={disableSubmit}
              >
                <Text style={[styles.buttonText, { color: disableSubmit && 'grey' || COLOR}]}>
                  {Platform.OS === 'ios' ? labelSubmit : labelSubmit.toUpperCase()}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,.3)',
  },
  blur: {
    position: 'absolute',
    backgroundColor: 'rgb(234,234,234)',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  content: Platform.select({
    ios: {
      width: 270,
      flexDirection: 'column',
      borderRadius: 13,
      overflow: 'hidden',
    },
    android: {
      flexDirection: 'column',
      borderRadius: 3,
      padding: 16,
      backgroundColor: 'white',
      overflow: 'hidden',
      elevation: 4,
      minWidth: 300,
    },
  }),
  header: Platform.select({
    ios: {
      margin: 18,
    },
    android: {
      margin: 12,
    },
  }),
  footer: Platform.select({
    ios: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      borderTopColor: '#A9ADAE',
      borderTopWidth: StyleSheet.hairlineWidth,
      height: 46,
    },
    android: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-end',
      marginTop: 4,
    },
  }),
  buttonSeparator: {
    height: '100%',
    backgroundColor: '#A9ADAE',
    width: StyleSheet.hairlineWidth,
  },
  title: Platform.select({
    ios: {
      color: 'black',
      textAlign: 'center',
      fontSize: 18,
      fontWeight: '600',
    },
    android: {
      color: '#33383D',
      fontWeight: '500',
      fontSize: 18,
    },
  }),
  description: Platform.select({
    ios: {
      textAlign: 'center',
      color: 'black',
      fontSize: 13,
      marginTop: 4,
    },
    android: {
      color: '#33383D',
      fontSize: 16,
      marginTop: 10,
    },
  }),
  textInputWrapper: Platform.select({
    ios: {
      backgroundColor: 'white',
      borderWidth: StyleSheet.hairlineWidth,
      borderRadius: 6,
      borderColor: '#A9ADAE',
      marginHorizontal: 20,
      marginBottom: 20,
      paddingHorizontal: 8,
    },
    android: {
      marginHorizontal: 10,
      marginBottom: 20,
    },
  }),
  textInput: Platform.select({
    ios: {
      height: 32,
      color: 'black'
    },
    android: {
      marginLeft: -4,
      paddingLeft: 4,
      height: 40,
      color: 'black'
    },
  }),
  button: Platform.select({
    ios: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    android: {
      justifyContent: 'center',
      alignItems: 'center',
    },
  }),
  buttonText: Platform.select({
    ios: {
      textAlign: 'center',
      fontSize: 17,
      fontWeight: '600',
      backgroundColor: 'transparent',
      color: COLOR,
    },
    android: {
      textAlign: 'center',
      backgroundColor: 'transparent',
      fontWeight: '600',
      padding: 8,
      fontSize: 14,
      color: COLOR,
    },
  }),
});

InputDialog.propTypes = {
  disableSubmit: PropTypes.bool.isRequired,
  visible: PropTypes.bool.isRequired,
  title: PropTypes.string,
  description: PropTypes.string,
  placeholder: PropTypes.string,
  labelCancel: PropTypes.string,
  labelSubmit: PropTypes.string,
  onCancelPress: PropTypes.func.isRequired,
  onChangeText: PropTypes.func.isRequired,
  onSubmitPress: PropTypes.func.isRequired,
};

InputDialog.defaultProps ={
  title: EN.game__prompt_title,
  description: EN.game__prompt_description,
  placeholder: '',
  labelCancel: EN.button__cancel,
  labelSubmit: EN.button__submit,
}

export { InputDialog };
