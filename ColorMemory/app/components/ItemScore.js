import React from 'react';
import {
    Text,
    StyleSheet,
    View,
    ViewPropTypes,
} from 'react-native';
import PropTypes from 'prop-types';

class ItemScore extends React.PureComponent {
    render() {
        const { rank, name, score, containerStyle, textStyle } = this.props;
        return (
            <View style={[styles.container, containerStyle]}>
                <Text style={[styles.rank, textStyle]}>{rank}</Text>
                <Text
                    numberOfLines={1}
                    ellipsizeMode="tail"
                    style={[styles.name, textStyle]}
                >
                    {name}
                </Text>
                <Text style={[styles.score, textStyle]}>{score}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: 40,
        alignItems: 'center'
    },
    rank: {
        flex: 1,
        textAlign: 'center',
        color: 'black'
    },
    name: {
        flex: 5,
        textAlign: 'center',
        color: 'black'
    },
    score: {
        flex: 1,
        textAlign: 'center',
        textAlignVertical: 'center',
        color: 'black'
    },
});

export { ItemScore };

ItemScore.propTypes = {
    containerStyle: ViewPropTypes.style,
    textStyle: ViewPropTypes.style,
    rank: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    score: PropTypes.number.isRequired,
  };
  
ItemScore.defaultProps = {
    containerStyle: null,
    textStyle: null,
}