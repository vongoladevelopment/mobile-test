import React from 'react';
import {
    Image,
    TouchableOpacity,
    View,
    StyleSheet,
} from 'react-native';
import CardFlip from 'react-native-card-flip';
import PropTypes from 'prop-types';

import { cardBack } from '../images';

class Card extends React.PureComponent {
    _onPress = () => {
        this.props.onPress(this.props.id);
    }
    
    componentWillReceiveProps (nextProps) {
        if(nextProps.flipped !== this.props.flipped) {
            if (this.card != null) this.card.flip()
        }
    }

    render() {
        const { image, visible } = this.props;
        if (!visible) return null;

        return (
            <CardFlip
                style={styles.container}
                ref={(card) => this.card = card}
                duration={500}
            >
                <TouchableOpacity onPress={this._onPress} >
                    <View>
                        <Image
                            source={cardBack}
                            resizeMode="contain"
                            style={styles.card}
                        />
                    </View>
                </TouchableOpacity>
                <Image
                    source={image}
                    resizeMode="contain"
                    style={styles.card}
                />
            </CardFlip>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%'
    },
    card: {
        aspectRatio: 1,
        height: '100%'
    },
})

export { Card };

Card.propTypes = {
    id: PropTypes.number.isRequired,
    image: PropTypes.number.isRequired,
    visible: PropTypes.bool.isRequired,
    flipped: PropTypes.bool.isRequired,
    onPress: PropTypes.func.isRequired,
  };