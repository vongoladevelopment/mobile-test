import React, { PureComponent } from 'react';

import {
    TouchableOpacity,
    Text,
    View,
    StyleSheet,
} from 'react-native';

class HeaderButton extends PureComponent {
    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress}>
                <View style={styles.container}>
                    <Text style={styles.text}>{this.props.text}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: 36,
        paddingHorizontal: 16,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        fontSize: 10,
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#037aff'
    }
})

export { HeaderButton };
