import React from 'react';
import { createStackNavigator } from 'react-navigation';
import { Image, StyleSheet, Text } from 'react-native';
import { HeaderButton } from '../components';

import { logo } from '../images';
import EN from '../locales/en';

import GamePage from '../pages/game/GamePage';
import ScoresPage from '../pages/scores/ScoresPage';

export default createStackNavigator({
        Game: {
            screen: GamePage,
            navigationOptions: ({ navigation }) => ({
                headerStyle: styles.header,
                headerTitleStyle: styles.headerTitle,
                headerRight: (
                    <HeaderButton
                        text={EN.button__high_scores}
                        onPress={() => navigation.navigate('Scores')}/>
                    ),
                headerLeft: (
                    <Image source={logo} style={styles.logo} resizeMode="contain"/>
                ),
                headerBackTitle: null,
            })
        },
        Scores: {
            screen: ScoresPage,
            navigationOptions: () => ({
                headerStyle: styles.header,
                headerTitle: EN.title__high_score,   
            }),
        }
    }, {
        initialRouteName: 'Game',
        navigationOptions: {
            gesturesEnabled: false,
        },
    }
);

const styles = StyleSheet.create({
    header: {
        backgroundColor: 'white',
        borderBottomWidth: 0,
        textAlign:'center',
    },
    headerTitle: {
        textAlign:'center',
        alignSelf:'center',
        flex:1,
    },
    logo: {
        height: 40, 
        width: 40,
        marginLeft: 16,
    },
    score: {
        fontSize: 14,
        fontWeight: 'bold',
    }
});
