export const ADD_SCORE = 'ADD_SCORE';
export const ADD_SCORE_SUCCESS = 'ADD_SCORE_SUCCESS';
export const ADD_SCORE_FAIL = 'ADD_SCORE_FAIL';


export const PERSIST_SCORES = 'PERSIST_SCORES';
export const READ_PERSISTED_STORAGE = 'READ_PERSISTED_STORAGE';
export const READ_PERSISTED_STORAGE_SUCCESS = 'READ_PERSISTED_STORAGE_SUCCESS';
export const SET_PERSISTED_STORAGE = 'SET_PERSISTED_STORAGE';

export const addScore = (name, score) => ({
  type: ADD_SCORE,
  name,
  score,
});
