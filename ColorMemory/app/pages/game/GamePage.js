/**
 * Game Page
 *
 * @format
 * @flow
 */

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { FlatList, StyleSheet, View } from 'react-native';
import { Card, InputDialog } from '../../components';
import CARDS from '../../cards';
import EN from '../../locales/en';
import { addScore } from '../../actions/score';
import { selectScores } from '../../selectors/data';

const initState = {
  selected: [],
  completed: [],
  scores: 0,
  showInputDialog: false,
  name: '',
  description: '',
};

export class GamePage extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    const score = navigation.getParam('scores', 0);
    return {
      title: `${EN.game__label_score} ${score}`,
    };
  };

  constructor(props) {
    super(props);
    var cardArr = this._shuffle([...CARDS, ...CARDS]);

    this.state = { 
      ...initState,
      cardArr,
    };
  }

  _shuffle = (array) => {
    let i = array.length;
    while(i !== 0) {
      var randomIndex = Math.floor(Math.random() * i);
      i -= 1;

      // swap element
      const tempValue = array[i];
      array[i] = array[randomIndex];
      array[randomIndex] = tempValue;
    }

    return array;
  }

  _restartGame = () => {
    this.setState({ 
      ...initState,
      cardArr: this._shuffle([...CARDS, ...CARDS]),
    });
    this.props.navigation.setParams({ scores: 0 })
  }

  _checkFlippedCard = () => {
    const { cardArr, selected, completed, scores } = this.state;
    if (selected.length == 2) {
      const isMatch = cardArr[selected[0]].value === cardArr[selected[1]].value;
      if (isMatch) {
        this.setState({ 
          selected: [],
          completed: [...completed, ...selected],
          scores: scores + 5,
        }, this._updateScore);
      } else {        
        this.setState({ selected: [], scores: scores -1 }, this._updateScore);
      }
    } else if (selected.length > 2) {
      console.warn("_checkFlippedCard", "card size > 2")
    }
  }

  _updateScore = () => {
    const { scores, cardArr, completed } = this.state;
    const { navigation, scoresList } = this.props
    navigation.setParams({ scores })
    if (cardArr.length === completed.length) {
      // Game end, get current ranking.
      let i = 0;
      let rank = -1;
      while(i < scoresList.size && rank === -1) {
        if (scores >= scoresList.getIn([i, 'score'])) {
          rank = scoresList.getIn([i, 'rank']);
        }
        i++;
      }
      if (rank === -1) {
        rank = scoresList.size > 0 ? scoresList.last().get('rank') + 1 : 1;
      } 
      this.setState({ showInputDialog: true, description: `Your final score is ${scores} and rank #${rank}` });
    }
  }

  _onCardPress = (index) => {
    const { selected } = this.state;
    // Card is already flipped or more than 2 cards has flipped.
    if (selected.includes(index) || selected.length > 1) return;
    // Update selected card and check for match.
    const newSelected = [...selected, index];
    this.setState({ selected: newSelected }, () => {
      if (newSelected.length === 2) setTimeout(this._checkFlippedCard, 1300) 
    });
  }

  _renderItem = ({ item, index }) => {
    const gameComplete = this.state.completed.length === this.state.cardArr.length;
    console.log(this.state.itemHeight)
    return (
    <View style={[styles.cardContainer, { height: this.state.itemHeight, width: this.state.itemHeight }]}>
      <Card
        id={index}
        onPress={this._onCardPress}
        flipped={this.state.selected.includes(index)}
        visible={!this.state.completed.includes(index) || gameComplete}
        image={item.image}
      />
    </View>
  )};
  
  _onChangeText = (name) => this.setState({ name });

  _onSubmitName = () => {
    const { name, scores } = this.state;
    this.props.addScore(name, scores)
    this._restartGame();
  }

  _onCancelName = () => this._restartGame();
  
  /**
   * Measure window dimension to calculate width for each card.
   * Cards group width is limited by window width(height) in portarit(landscape) mode.
   */
  _onLayout = ({ nativeEvent }) => {
    const { height, width } = nativeEvent.layout;
    let groupWidth = height > width ? width : height;
    // Add padding to the card group.
    groupWidth -= 32;
    this.setState({ itemHeight: groupWidth / 4 });
  }

  render() {
    return (
      <View style={styles.container} onLayout={this._onLayout} >
        <InputDialog
          visible={this.state.showInputDialog}
          disableSubmit={this.state.name.trim().length === 0}
          onChangeText={this._onChangeText}
          onCancelPress={this._onCancelName}
          onSubmitPress={this._onSubmitName}
          description={this.state.description}
        />
        <View style={styles.spacer}/>
        <FlatList
          numColumns={4}
          data={this.state.cardArr}
          renderItem={this._renderItem}
          style={styles.list}
          keyExtractor={(item, index) => `${index}::${item.value}`}
          extraData={this.state}
          scrollEnabled={false}
        />
        <View style={styles.spacer}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  list: {
    flexGrow: 0,
  },
  spacer: {
    flex: 1,
  },
  cardContainer: {
    padding: 4,
    flexDirection: 'row',
  },
});

const mapStateToProps = state => ({
  scoresList: selectScores(state),
})

const mapDispatchToProps = (dispatch) => ({
  addScore: (name, score) => dispatch(addScore(name, score)),
})

export default connect(mapStateToProps, mapDispatchToProps)(GamePage);