/**
 * Scores Page shows high scores table with 3 columns: 
 * rank, player name & scores.
 * 
 * @format
 * @flow
 */

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { FlatList, StyleSheet, View, Text } from 'react-native';
import { ItemScore } from '../../components';
import { selectScores } from '../../selectors/data';
import EN from '../../locales/en';

export class ScoresPage extends PureComponent {
  _renderItem = ({ item, index }) => (
    <ItemScore
      rank={`#${item.get('rank')}`}
      name={item.get('name')}
      score={item.get('score')}
    />
  );

  _renderListHeader = () => (
    <View>
      <ItemScore
        rank={EN.high_scores__rank}
        name={EN.high_scores__name}
        score={EN.high_scores__score}
        containerStyle={styles.headerContainerStyle}
        textStyle={styles.headerTextStyle}
      />
    </View>
  )

  _renderListEmptyComponent = () => (
    <View style={styles.placeholderContainer}>
      <Text style={styles.placeholder}>{EN.high_scores__placeholder}</Text>
    </View>
  )

  _keyExtractor = (item, index) => `${index}::${item.get('rank')}::${item.get('name')}`;

  render() {
    const { scores } = this.props;
    return (
      <View style={styles.container} >
        <FlatList 
          data={scores && scores.toArray()}
          renderItem={this._renderItem}
          ListHeaderComponent={this._renderListHeader}
          style={styles.list}
          keyExtractor={this._keyExtractor}
          ListEmptyComponent={this._renderListEmptyComponent}
          contentContainerStyle={{ flexGrow: 1 }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  list: {
    width: '100%',
    paddingHorizontal: 16,
  },
  headerContainerStyle: {
    borderBottomWidth: 1.5,
    borderBottomColor: 'black',
  },
  headerTextStyle: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'black',
  },
  placeholderContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  placeholder: {
    color: 'black',
  },
});

const mapStateToProps = (state) => {
  return ({
    scores: selectScores(state),
  });
}

export default connect(mapStateToProps, null)(ScoresPage);