import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { all } from 'redux-saga/effects';

import data from './reducers/data';
import { READ_PERSISTED_STORAGE } from './actions/score';
import persistStorageSaga from './sagas/persistStorageSaga';
import scoresSaga from './sagas/scores';

const sagaMiddleware = createSagaMiddleware();
const rootReducer = combineReducers({
  data, 
});

/* eslint-disable no-underscore-dangle */
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(sagaMiddleware)));

function* rootSaga() {
  yield all([
    persistStorageSaga,
    scoresSaga,
  ]);
}

sagaMiddleware.run(rootSaga);

// Write persisted data into store
store.dispatch({ type: READ_PERSISTED_STORAGE });

export default store;
