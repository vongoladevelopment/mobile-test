export const logo = require('./logo.png');
export const cardBack = require('./card_background.png');

export const cardTV = require('./card_tv.png');
export const cardMultiScreen = require('./card_multiscreen.png');
export const cardRemote = require('./card_remote.png');
export const cardCloud = require('./card_cloud.png');
export const cardTablet = require('./card_tablet.png');
export const cardVR = require('./card_vr.png');
export const cardCC = require('./card_cc.png');
export const cardConsole = require('./card_console.png');
