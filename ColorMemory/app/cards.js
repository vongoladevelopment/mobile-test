import { 
    cardCC,
    cardCloud,
    cardConsole,
    cardRemote,
    cardMultiScreen,
    cardTablet,
    cardTV,
    cardVR,
} from './images';

export default [
    {
        value: 'cc',
        image: cardCC,
    },
    {
        value: 'cloud',
        image: cardCloud,
    },
    {
        value: 'console',
        image: cardConsole,
    },
    {
        value: 'multiscreen',
        image: cardMultiScreen,
    },
    {
        value: 'remote',
        image: cardRemote,
    },
    {
        value: 'tablet',
        image: cardTablet,
    },
    {
        value: 'tv',
        image: cardTV,
    },
    {
        value: 'vr',
        image: cardVR,
    },
];