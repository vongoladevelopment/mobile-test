# accedo-mobile-test

## 1. Installtion
### via APK/ipa
- Android: Download 'app-release.apk' in the project root and install on your device.
- iOS: As my apple developer has been expired, ipa is not provided.

### via command
1. ```cd ColorMemory && npm install```

2. ```react-native run-ios``` or ```react-native run-android```



## 2. Work Sample
The goal of this work sample is to construct a sample memory game called �Color Memory�. The game board consists of a 4x4 grid with 8 pairs of color cards. The game
starts initially with all cards facing down. The player is to then flip two cards each round, trying to find a match. If the flipped pair is a match, the player receives five (5) points, and the cards may be removed from the game board. Otherwise, the cards are turned facedown again and the player loses one (1) point. This continues until all pairs have
been found.  After the game is finished, the user should be prompted to input his name. The user's name and the score would then be stored in a database, and the user should be notified of his score and the current rankings.

## 3. Specifications and Features
* The app should be written using React Native of your selected target platform.
* For Android, it must support devices running Android 6.0 and above
* For iOS, it must support devices running iOS 10.0 and above.
* The app should run on both phone and tablet devices
* The app must at least be runnable on an emulator. For Android, it should be able to run on a device as well.
* The app should support rotation on all screens.
* After each round, a brief one (1) second pause should be implemented before scoring to allow the player to see what the second selected card is.
* When a user submits his name, the entered value must pass basic validation, in order to continue.
* Clicking the High Scores button will take the user to the table of high scores.
* The high scores table may contain negative numbers.
* A score is considered a high score if it is higher than any of the current high scores OR there is space in the table.
* High scores must be stored and persisted as long as the user does not uninstall the app or clear the app's stored data.
* High scores should be stored locally on the device.

## 4. Design
* The full game must fit exactly inside a device's available window area.
* The logo must be displayed in the top left of the window.
* The High Scores button must be visible to the top right of the window.
* The current score should be displayed in the top, centered between the logo and the High Score button.
* The game board must be displayed below the logo and high score button
* The user input may be implemented as a separate screen or as a popup
* The high scores table must be displayed as a proper table with three columns: Rank, Name, and Score
* All necessary graphics for the cards, logo, and app icon have been supplied.
* Usage of any other graphical elements is allowed, but not required.